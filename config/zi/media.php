<?php

return [
    "image"=>[
        'sizes'     => [],//null is any size
        'functions' => [ 'crop', 'resize', 'fit','max' ],//max function support : crop, resize, fit,max
    ],
    'max_upload_size'=>-1,//-1 unlimited, KB (example: 1024 <=> 1MB)
    'mime_type'=>'mimes:txt,jpeg,jpg,png,gif,pdf,doc,docx,xls,xlsx,ppt,pptx,zip,rar,json,yaml',//null || empty => allow all
];


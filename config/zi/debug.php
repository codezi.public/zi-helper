<?php

return [
    'telegram_bot_token'       => env('TELEGRAM_BOT_TOKEN', ''),
    'telegram_chat_id'         => env('TELEGRAM_BUG_GROUP_CHAT_ID', ''),

    #Sentry config
    'sentry'                   => '',
    'sentry_debug_query_url'   => '',
    'sentry_js_tracking_error' => '',
];

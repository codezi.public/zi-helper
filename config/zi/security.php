<?php
return [
    'db_password' => [
        'key'    => env('DB_PASSWORD_SECURITY_KEY'),
        'cipher' => 'AES-128-CBC',
    ],

    'db_email' => [
        'key'    => env('DB_EMAIL_SECURITY_KEY'),
        'cipher' => 'AES-128-CBC',
    ],

    'db_phone' => [
        'key'    => env('DB_PHONE_SECURITY_KEY'),
        'cipher' => 'AES-128-CBC',
    ],

    'db_other' => [
        'key'    => env('DB_OTHER_SECURITY_KEY'),
        'cipher' => 'AES-128-CBC',
    ],


    'db_config'                => [
        'key'    => env('DB_CONFIG_SECURITY_KEY'),
        'cipher' => 'AES-128-CBC',
    ],


    //These keys are considered sensitive during json output, it will be hidden value if it belongs to this list, for example see log request, api response when debugging
    'json_key_filter_security' => [
        'php-auth-user', 'php-auth-pw', 'authorization',
        'token', 'signature',
        'apikey', "checksum",
        "access_token",
        "refresh_token"
    ]
];

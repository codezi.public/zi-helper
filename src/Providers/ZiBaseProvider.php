<?php

namespace ZiBase\Providers;

use App\Models\User;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;
use ZiBase\Console\Doctor;
use ZiBase\Console\Help;
use ZiBase\Console\Install;
use ZiBase\Console\SetupAdminAccount;
use ZiBase\Helpers\ZiCmsHelper;
use ZiBase\Models\Content\ZiCategoryModel;
use ZiBase\Models\Content\ZiFAQModel;
use ZiBase\Models\Content\ZiTagsModel;
use ZiBase\Models\ZiRole;

class ZiBaseProvider extends ServiceProvider
{

    /**
     * @return void
     */
    public function boot()
    {


        if ($this->app->runningInConsole()) {
            #region load config
            $this->publishes([
                __DIR__ . '/../../config/zi/debug.php'    => config_path('zi/debug.php'),
                __DIR__ . '/../../config/zi/security.php' => config_path('zi/security.php'),
                __DIR__ . '/../../config/zi/media.php'    => config_path('zi/media.php'),
                __DIR__ . '/../../config/zi/seo.php'      => config_path('zi/seo.php'),
            ], 'zi');
        }


    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../../config/zi/debug.php', 'zi.debug');
        $this->mergeConfigFrom(__DIR__ . '/../../config/zi/security.php', 'zi.security');
        $this->mergeConfigFrom(__DIR__ . '/../../config/zi/media.php', 'zi.media');
        $this->mergeConfigFrom(__DIR__ . '/../../config/zi/seo.php', 'zi.seo');

    }
}
